#!/sbin/sh
# --------------------------------
# TGP INSTALLER v12.1.0
# Created by @djb77
#
# Credit also goes to @lyapota
# @farovitus, @Morogoku, @dwander,
# @Chainfire, @osm0sis, @Tkkg1994,
# @ambasadii, , @~clumsy~, @mwilky,
# and @topjohnwu for code snippets.
# --------------------------------

# Read option number from updater-script
OPTION=$1

# Block location
BLOCK=/dev/block/platform/11120000.ufs/by-name

# Variables
TGPTEMP=/tmp/tgptemp
AROMA=/tmp/aroma
TGP=/data/media/0/TGP
CONFIG=$TGP/config
BUILDPROP=/system/build.prop
PROPDEFAULT=/system/etc/prop.default
VENDORBUILDPROP=/vendor/build.prop
VENDORDEFAULTPROP=/vendor/default.prop
FLOATINGFEATURE=/system/etc/floating_feature.xml
KERNEL_REMOVE="init.services.rc init.PRIME-Kernel.rc init.spectrum.sh init.spectrum.rc init.primal.rc init.noto.rc kernelinit.sh wakelock.sh super.sh cortexbrain-tune.sh spectrum.sh kernelinit.sh spa init_d.sh initd.sh moro-init.sh sysinit.sh tgpkernel.sh noto.sh"

if [ $OPTION == "setup" ]; then
	## Set Permissions
	chmod 755 $AROMA/adb
	chmod 755 $AROMA/adb.bin
	chmod 755 $AROMA/fastboot
	chmod 755 $AROMA/busybox
	chmod 755 $AROMA/fstrim
	chmod 755 $AROMA/tar
	chmod 755 $AROMA/zip
	chmod 755 $AROMA/tgp.sh
	exit 10
fi

if [ $OPTION == "config_check" ]; then
	## Config Check
	# If config backup is present, alert installer
	mount /data
	if [ -e $CONFIG/tgp-backup.prop ]; then
		echo "install=1" > $AROMA/backup.prop
	fi
	exit 10
fi

if [ $OPTION == "config_backup" ]; then
	## Backup Config
	# Check if TGP folder exists on Internal Memory, if not, it is created
	if [ ! -d $TGP ]; then
		mkdir $TGP
		chmod 777 $TGP
	fi
	# Check if config folder exists, if it does, delete it 
	if [ -d $CONFIG-backup ]; then
		rm -rf $CONFIG-backup
	fi
	# Check if config folder exists, if it does, ranme to backup
	if [ -d $CONFIG ]; then
		mv -f $CONFIG $CONFIG-backup
	fi
	# Check if config folder exists, if not, it is created
	if [ ! -d $CONFIG ]; then
		mkdir $CONFIG
		chmod 777 $CONFIG
	fi
	# Copy files from $AROMA to backup location
	cp -f $AROMA/* $CONFIG
	# Delete any files from backup that are not .prop files
	find $CONFIG -type f ! -iname "*.prop" -delete
	# Remove unwanted .prop files from the backup
	cd $CONFIG
	[ -f "$CONFIG/g960f.prop" ] && rm -f $CONFIG/g960f.prop
	[ -f "$CONFIG/g960n.prop" ] && rm -f $CONFIG/g960n.prop
	[ -f "$CONFIG/g960x.prop" ] && rm -f $CONFIG/g960x.prop
	[ -f "$CONFIG/g965f.prop" ] && rm -f $CONFIG/g965f.prop
	[ -f "$CONFIG/g965n.prop" ] && rm -f $CONFIG/g965n.prop
	[ -f "$CONFIG/g965x.prop" ] && rm -f $CONFIG/g965x.prop
	[ -f "$CONFIG/g96xf.prop" ] && rm -f $CONFIG/g96xf.prop
	[ -f "$CONFIG/g96xn.prop" ] && rm -f $CONFIG/g96xn.prop
	[ -f "$CONFIG/g96xx.prop" ] && rm -f $CONFIG/g96xx.prop
	for delete_prop in *.prop 
	do
		if grep "item" "$delete_prop"; then
			rm -f $delete_prop
		fi
		if grep "install=0" "$delete_prop"; then
			rm -f $delete_prop
		fi 
	done
	exit 10
fi

if [ $OPTION == "config_restore" ]; then
	## Restore Config
	# Copy backed up config files to $AROMA
	cp -f $CONFIG/* $AROMA
	exit 10
fi

if [ $OPTION == "wipe_magisk" ]; then
	## Wipe old Magisk / SuperSU Installs (@mwilky)
	mount /cache
	rm -rf /cache/magisk.log /cache/last_magisk.log /cache/magiskhide.log \
		 /cache/.disable_magisk /cache/magisk /cache/magisk_merge /cache/magisk_mount \
		 /cache/unblock /cache/magisk_uninstaller.sh /data/Magisk.apk /data/magisk.apk \
		 /data/magisk.img /data/magisk_merge.img /data/busybox /data/magisk /data/custom_ramdisk_patch.sh 2>/dev/null
	rm -rf /cache/.supersu /cache/su.img /cache/SuperSU.apk \
	     /data/.supersu /data/stock_boot_*.img.gz /data/su.img \
	     /data/SuperSU.apk /data/app/eu.chainfire.supersu* \
	     /data/data/eu.chainfire.supersu /data/supersu /supersu
	exit 10
fi

if [ $OPTION == "check_g960f" ]; then
	## Variant Check for G960F
	echo "install=1" > $AROMA/g960f.prop
	echo "install=1" > $AROMA/g960x.prop
	echo "install=1" > $AROMA/g96xf.prop
	echo "install=1" > $AROMA/g96xx.prop
	exit 10
fi

if [ $OPTION == "check_g960n" ]; then
	## Variant Check for G960N
	echo "install=1" > $AROMA/g960n.prop
	echo "install=1" > $AROMA/g960x.prop
	echo "install=1" > $AROMA/g96xn.prop
	echo "install=1" > $AROMA/g96xx.prop
	exit 10
fi

if [ $OPTION == "check_g965f" ]; then
	## Variant Check for G965F
	echo "install=1" > $AROMA/g965f.prop
	echo "install=1" > $AROMA/g965x.prop
	echo "install=1" > $AROMA/g96xf.prop
	echo "install=1" > $AROMA/g96xx.prop
	exit 10
fi

if [ $OPTION == "check_g965n" ]; then
	## Variant Check for G965F
	echo "install=1" > $AROMA/g965n.prop
	echo "install=1" > $AROMA/g965x.prop
	echo "install=1" > $AROMA/g96xn.prop
	echo "install=1" > $AROMA/g96xx.prop
	exit 10
fi

if [ $OPTION == "backup_efs" ]; then	
	## Backup EFS
	mount $BLOCK/EFS /efs
	if [ ! -d /data/media/0/TGP ];then
	mkdir /data/media/0/TGP
	chmod -R 777 /data/media/0/TGP
	fi
	if [ -e /data/media/0/TGP/efs.img.bak ]; then
	rm -f /data/media/0/TGP/efs.img.bak 
	fi
	if [ -e /data/media/0/TGP/efs.img ]; then
	cp -f /data/media/0/TGP/efs.img /data/media/0/TGP/efs.img.bak
	rm -f /data/media/0/TGP/efs.img
	fi
	dd if=$BLOCK/EFS of=/data/media/0/TGP/efs.img bs=4096
	exit 10
fi

if [ $OPTION == "wipe" ]; then
	## Full Wipe
	find /data -maxdepth 1 -type d ! -path "/data" ! -path "/data/media" | xargs rm -rf
	exit 10
fi

if [ $OPTION == "system_patch" ]; then
	## System Patches
	# Remove init.d and addon.d Placeholders
	rm -f /system/addon.d/placeholder
	rm -f /system/etc/init.d/placeholder
	# Delete Wakelock.sh 
	rm -f /magisk/phh/su.d/wakelock*
	rm -f /su/su.d/wakelock*
	rm -f /system/su.d/wakelock*
	rm -f /system/etc/init.d/wakelock*
	# build.prop Mods
	echo "# Fix Fingerprint Unlock" >> $BUILDPROP
	echo "fingerprint.unlock=1" >> $BUILDPROP
	echo "# Fix Mobile Hotspot" >> $BUILDPROP
	echo "net.tethering.noprovisioning=true" >> $BUILDPROP
	echo "# Fix FPS for Boot/Shutdown Animations" >> $BUILDPROP
	echo "boot.fps=25" >> $BUILDPROP
	echo "shutdown.fps=25" >> $BUILDPROP
	# Location Fixes
	if grep -q install=3 $AROMA/location.prop; then
		sed -i "s/G960F/G960N/g" $BUILDPROP $PROPDEFAULT /system/info.extra $FLOATINGFEATURE
		sed -i "s/G965F/G965N/g" $BUILDPROP $PROPDEFAULT /system/info.extra $FLOATINGFEATURE
	fi	
	exit 10
fi

if [ $OPTION == "odm_patch" ]; then
	## ODM Patches
	if grep -q install=1 $AROMA/g960x.prop; then
		sed -i -- 's/G965/G960/g' /odm/omc/CSCVersion.txt /odm/omc/SW_Configuration.xml
		find /odm -type f -name 'omc*' | xargs sed -i 's/SM-G965/SM-G960/g'
	fi
	if grep -q install=3 $AROMA/location.prop; then
		sed -i -- 's/G960F/G960N/g' /odm/omc/CSCVersion.txt /odm/omc/SW_Configuration.xml
		sed -i -- 's/G960U/G960N/g' /odm/omc/CSCVersion.txt /odm/omc/SW_Configuration.xml
		sed -i -- 's/G960W/G960N/g' /odm/omc/CSCVersion.txt /odm/omc/SW_Configuration.xml
		sed -i -- 's/G965F/G965N/g' /odm/omc/CSCVersion.txt /odm/omc/SW_Configuration.xml
		sed -i -- 's/G965U/G965N/g' /odm/omc/CSCVersion.txt /odm/omc/SW_Configuration.xml
		sed -i -- 's/G965W/G965N/g' /odm/omc/CSCVersion.txt /odm/omc/SW_Configuration.xml
		find /odm -type f -name 'omc*' | xargs sed -i 's/SM-G960F/SM-G960N/g'
		find /odm -type f -name 'omc*' | xargs sed -i 's/SM-G960U/SM-G960N/g'
		find /odm -type f -name 'omc*' | xargs sed -i 's/SM-G960W/SM-G960N/g'
		find /odm -type f -name 'omc*' | xargs sed -i 's/SM-G965F/SM-G965N/g'
		find /odm -type f -name 'omc*' | xargs sed -i 's/SM-G965U/SM-G965N/g'
		find /odm -type f -name 'omc*' | xargs sed -i 's/SM-G965W/SM-G965N/g'
	fi
	if grep -q install=4 $AROMA/location.prop; then
		sed -i -- 's/G960N/G960F/g' /odm/omc/CSCVersion.txt /odm/omc/SW_Configuration.xml
		sed -i -- 's/G960U/G960F/g' /odm/omc/CSCVersion.txt /odm/omc/SW_Configuration.xml
		sed -i -- 's/G960W/G960F/g' /odm/omc/CSCVersion.txt /odm/omc/SW_Configuration.xml
		sed -i -- 's/G965N/G965F/g' /odm/omc/CSCVersion.txt /odm/omc/SW_Configuration.xml
		sed -i -- 's/G965U/G965F/g' /odm/omc/CSCVersion.txt /odm/omc/SW_Configuration.xml
		sed -i -- 's/G965W/G965F/g' /odm/omc/CSCVersion.txt /odm/omc/SW_Configuration.xml
		find /odm -type f -name 'omc*' | xargs sed -i 's/SM-G960N/SM-G960F/g'
		find /odm -type f -name 'omc*' | xargs sed -i 's/SM-G960U/SM-G960F/g'
		find /odm -type f -name 'omc*' | xargs sed -i 's/SM-G960W/SM-G960F/g'
		find /odm -type f -name 'omc*' | xargs sed -i 's/SM-G965N/SM-G965F/g'
		find /odm -type f -name 'omc*' | xargs sed -i 's/SM-G965U/SM-G965F/g'
		find /odm -type f -name 'omc*' | xargs sed -i 's/SM-G965W/SM-G965F/g'
	fi
	exit 10
fi

if [ $OPTION == "vendor_patch" ]; then
	## Vendor Patches
	# S9 Variant Fixes
	if grep -q install=1 $AROMA/g960x.prop; then
		sed -i "s/ro.sf.lcd_density=480/ro.sf.lcd_density=420/g" $VENDORBUILDPROP
		sed -i "s/ro.sf.init.lcd_density=640/ro.sf.init.lcd_density=560/g" $VENDORBUILDPROP
	fi
	# Location Fixes
	if grep -q install=3 $AROMA/location.prop; then
		sed -i "s/G960F/G960N/g" $VENDORBUILDPROP
		sed -i "s/G965F/G965N/g" $VENDORBUILDPROP
	fi
	exit 10
fi

if [ $OPTION == "csc_setup" ]; then
	if [ ! -e "/efs/imei/mps_code.dat" ]; then
		mount /efs
	fi	
	ACTUAL_CSC=`cat /efs/imei/mps_code.dat`
	ACTUAL_OMC=`cat /efs/imei/omcnw_code.dat`
	ACTUAL_OMC2=`cat /efs/imei/omcnw_code2.dat`
	SALES_CODE=`cat /odm/omc/sales_code.dat`
	sed -i -- "s/CSC=//g" /tmp/aroma/csc.prop
	NEW_CSC=`cat /tmp/aroma/csc.prop`
	if [ $NEW_CSC == "Skip" ]; then
		NEW_CSC=$ACTUAL_CSC
	else
		echo "Changing CSC code to $NEW_CSC"
	fi
	sed -i -- "s/$ACTUAL_CSC/$NEW_CSC/g" /efs/imei/mps_code.dat
	sed -i -- "s/$ACTUAL_OMC/$NEW_CSC/g" /efs/imei/omcnw_code.dat
	sed -i -- "s/$ACTUAL_OMC2/$NEW_CSC/g" /efs/imei/omcnw_code2.dat
	sed -i -- "s/$SALES_CODE/$NEW_CSC/g" /odm/omc/sales_code.dat
	exit 10
fi

if [ $OPTION == "csc_check" ]; then
	# Find CSC ID and dump to a /tmp file
	if [ ! -e "/efs/imei/mps_code.dat" ]; then
		mount /efs
	fi	
	csc_id=`cat /efs/imei/mps_code.dat`
	echo $csc_id > /tmp/csc_id.prop
	if [[ -d "/odm/omc/$csc_id" || -d "/odm/omc/single/$csc_id" ]]; then
		echo "CSC code $csc_id found"
	else
		echo "WARNING: CSC code $csc_id not found. Will use XEU CSC"
	fi
	echo "install=1" > $AROMA/csc-check.prop
	exit 10
fi

if [ $OPTION == "csc_mod" ]; then
	# Feature XML Edit in directory (@~clumsy~, modified by @djb77)
	csc_id=`cat /tmp/csc_id.prop`
	pattern=cscfeature.xml
	feature=$2
	value=$3
	dir=/odm/omc/$csc_id/conf
	if [ -d $dir ]; then 
		for file in $(find $dir -name $pattern)
		do
			lineNumber=0
			lineNumber=`sed -n "/<${feature}>.*<\/${feature}>/=" $file`
			if [ $lineNumber > 0 ] ; then
				echo "Found feature $feature in line $lineNumber and changing it to ${value}"
				sed -i "${lineNumber} c<${feature}>${value}<\/${feature}>" $file
			else
				echo "Adding feature $feature to the feature set and changing it to ${value}"
				sed -i "/<\/FeatureSet>/i <${feature}>${value}<\/${feature}>" $file
			fi
		done
	fi
	dir=/odm/omc/single/$csc_id/conf
	if [ -d $dir ]; then 
		for file in $(find $dir -name $pattern)
		do
			lineNumber=0
			lineNumber=`sed -n "/<${feature}>.*<\/${feature}>/=" $file`
			if [ $lineNumber > 0 ] ; then
				echo "Found feature $feature in line $lineNumber and changing it to ${value}"
				sed -i "${lineNumber} c<${feature}>${value}<\/${feature}>" $file
			else
				echo "Adding feature $feature to the feature set and changing it to ${value}"
				sed -i "/<\/FeatureSet>/i <${feature}>${value}<\/${feature}>" $file
			fi
		done
	fi
	exit 10
fi

if [ $OPTION == "copy_aod_images" ]; then
	## Copy AOD Home button image over to temp location for APK Patcher
	image=$2
	if [ ! -d /tmp/apk_patch ]; then
		mkdir /tmp/apk_patch
		chmod 644 /tmp/apk_patch
	fi
	cp -rf /tmp/aod_images/$image/* /tmp/apk_patch
	exit 10
fi

if [ $OPTION == "apk_patch" ]; then
	## APK Patcher
	# APK Patching Script
	FUNC_PATCH()
	{
	echo "Patching $APKName"
	if [ $APKName == "framework-res" ]; then
		cp -f $APKLocation/$APKName.apk $APKPatch.zip
	else
		cp -f $APKLocation/$APKName/$APKName.apk $APKPatch.zip
	fi
	if [ $APKName == "AODService_v40" ]; then
		$AROMA/zip -d $APKPatch.zip res/drawable/ic_sysbar_home_aod.bmp
	fi
	cd $APKPatch
	$AROMA/zip -r -9 $APKPatch.zip *
	[ -f resources.arsc ] && $AROMA/zip -r -0 $APKPatch.zip resources.arsc
	rm -rf $APKLocation/$APKName/$APKName.apk
	if [ $APKName == "framework-res" ]; then
		cp -f $APKPatch.zip $APKLocation/$APKName.apk
	else
		cp -f $APKPatch.zip $APKLocation/$APKName/$APKName.apk
	fi
	}
	# Patch AODService_v40.apk
	APKLocation=/system/priv-app
	APKName=AODService_v40
	APKPatch=/tmp/apk_patch/$APKName
	[ -d $APKPatch ] && FUNC_PATCH
	# Patch SystemUI.apk
	APKLocation=/system/priv-app
	APKName=SystemUI
	APKPatch=/tmp/apk_patch/$APKName
	[ -d $APKPatch ] && FUNC_PATCH
	# Patch ThemeCenter.apk
	APKLocation=/system/priv-app
	APKName=ThemeCenter
	APKPatch=/tmp/apk_patch/$APKName
	[ -d $APKPatch ] && FUNC_PATCH
	# Patch wallpaper-res.apk
	APKLocation=/system/priv-app
	APKName=wallpaper-res
	APKPatch=/tmp/apk_patch/$APKName
	[ -d $APKPatch ] && FUNC_PATCH
	exit 10
fi

if [ $OPTION == "kernel_flash" ]; then
	## Set Kernel
	image=$2
	## Flash Kernel (@dwander)
	# Clean up old kernels
	for i in $KERNEL_REMOVE; do
		if test -f $i; then
			[ -f $1 ] && rm -f $i
			[ -f sbin/$1 ] && rm -f sbin/$i
			sed -i "/$i/d" init.rc 
			sed -i "/$i/d" init.samsungexynos8910.rc 
		fi
		if test -f sbin/$i; then
			[ -f sbin/$1 ] && rm -f sbin/$i
			sed -i "/sbin\/$i/d" init.rc 
			sed -i "/sbin\/$i/d" init.samsungexynos8910.rc 
		fi
	done
	for i in $(ls ./res); do
		test $i != "images" && rm -R ./res/$i
	done
	[ -f /system/bin/uci ] && rm -f /system/bin/uci
	[ -f /system/xbin/uci ] && rm -f /system/xbin/uci
	# Flash new Image
	if grep -q install=1 $AROMA/g960x.prop; then
		dd if=$TGPTEMP/kernels/$image-960.img of=$BLOCK/BOOT
	fi
	if grep -q install=1 $AROMA/g965x.prop; then
		dd if=$TGPTEMP/kernels/$image-965.img of=$BLOCK/BOOT
	fi
	sync
	exit 10
fi

if [ $OPTION == "splash_flash" ]; then
	## Custom Splash Screen (@Tkkg1994)
	cd /tmp/splash
	mkdir /tmp/splashtmp
	cd /tmp/splashtmp
	$AROMA/tar -xf $BLOCK/UP_PARAM
	cp /tmp/splash/logo.jpg .
	chown root:root *
	chmod 444 logo.jpg
	touch *
	$AROMA/tar -pcvf ../new.tar *
	cd ..
	cat new.tar > $BLOCK/UP_PARAM
	cd /
	rm -rf /tmp/splashtmp
	rm -f /tmp/new.tar
	sync
	exit 10
fi

if [ $OPTION == "adb" ]; then
	## Install ADB
	rm -f /system/xbin/adb /system/xbin/adb.bin /system/xbin/fastboot
	cp -f $AROMA/adb /system/xbin/adb
	cp -f $AROMA/adb.bin /system/xbin/adb.bin
	cp -f $AROMA/fastboot /system/xbin/fastboot
	chown 0:0 "/system/xbin/adb" "/system/xbin/adb.bin" "/system/xbin/fastboot"
	chmod 755 "/system/xbin/adb" "/system/xbin/adb.bin" "/system/xbin/fastboot"
	exit 10
fi

if [ $OPTION == "busybox" ]; then
	## Install Busybox
	rm -f /system/bin/busybox /system/xbin/busybox
	cp -f $AROMA/busybox /system/xbin/busybox
	exit 10
fi

if [ $OPTION == "deleteprop" ]; then
	## Delete prop entries
	propfile=$2
	prop=$3
	if grep -q "$prop" "$propfile"; then
		sed -i "/${prop}/d" $propfile
		echo "Removed $prop from $propfile"   
	else
		echo "$prop does not exist in $propfile"   
	fi
	exit 10
fi

if [ $OPTION == "setprop" ]; then
	## Add prop entries
	propfile=$2
	prop=$3
	arg1=$4
	arg2=$5
	if grep -q "$prop=$arg2" "$propfile"; then
		echo "$prop alredy set"
	else
		sed -i "/${prop}/d" $propfile
		echo "$prop=$arg2" >> $propfile
		echo "Changed $prop to $arg1 in build.prop"
	fi
	exit 10
fi

if [ $OPTION == "ffmod" ]; then
	## floating_features.xml Modifier
	arg1=$2
	arg2=$3
	sed -i "/${arg1}/d" $FLOATINGFEATURE
	echo "Removed $arg1 from $FLOATINGFEATURE"
	sed -i "/<SecFloatingFeatureSet>/ a\    <$arg1>$arg2</$arg1>" $FLOATINGFEATURE
	echo "Set $arg1 to $arg2 in $FLOATINGFEATURE"
	exit 10
fi

if [ $OPTION == "insert" ]; then
	## Insert line (originally from AK2 by osm0sis, simplified by @djb77)
	# <file> <if search string/inserted line> <before|after> <line match string>
	file=$2
	line_add=$3
	ba=$4
	line_match=$5
	if [ -z "$(grep "$line_add" $file)" ]; then
		case $ba in
			before) offset=0;;
			after) offset=1;;
		esac
		line=$((`grep -n "$line_match" $file | head -n1 | cut -d: -f1` + $offset))
		sed -i "${line}s;^;"${line_add}"\n;" $file
	fi
fi

