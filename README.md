# The Galaxy Project for Samsung Galaxy S9 / S9+

![TGP Logo](https://gitlab.com/djb77/tgp-s9-tw9/raw/master/tools/logo.jpg)

## Compatible with the following variants
- G960F
- G960FD
- G965F
- G965FD

## How to check for updates via git

Use the "git pull" command to keep the repository up-to-date

## How to build a flashable .zip (Linux only)

Execute the "build.sh" file via Terminal and wait for it to be compressed
